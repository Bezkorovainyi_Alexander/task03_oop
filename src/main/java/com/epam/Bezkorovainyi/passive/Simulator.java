package com.epam.Bezkorovainyi.passive;

public class Simulator {
    public Simulator() {
    }

    public Simulator(String name, String time, String type) {
        this.name = name;
        this.time=time;
        this.type=type;
    }

    @Override
    public String toString() {
        return "Simulator{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                ", type='" + type + '\'' +
                '}';

    }

    public String name;

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    String time;
    String type;
}
