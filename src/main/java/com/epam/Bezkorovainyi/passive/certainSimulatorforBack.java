package com.epam.Bezkorovainyi.passive;

public class certainSimulatorforBack extends Simulator{
    String num;
    String text;

    public certainSimulatorforBack(String name, String time, String type, String num, String text) {
        super(name, time, type);
        this.num = num;
        this.text=text;
    }

    @Override
    public String toString() {
        return "certainSimulatorforBack{"+
                "num='" + num + '\'' +
                ", text='" + text + '\'' +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
